FROM node:16

ARG NODE_ENV
ENV NODE_ENV $NODE_ENV

RUN mkdir -p /opt/app
WORKDIR /opt/app
COPY . /opt/app

RUN npm install

ENV PORT 5000
EXPOSE $PORT
CMD [ "npm", "start" ]
