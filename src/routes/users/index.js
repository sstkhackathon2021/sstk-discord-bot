import express from 'express';

const router = express.Router();

router.get('/:name', (req, res) => {
  res.send(`hello there ${req.params.name || 'user'}`);
});

export default router;
