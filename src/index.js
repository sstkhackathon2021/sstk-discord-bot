import createError from 'http-errors';
import express, { json, urlencoded } from 'express';
// import cookieParser from 'cookie-parser';
import logger from 'morgan';

import indexRouter from './routes/home';
import usersRouter from './routes/users';
import hcRouter from './routes/healtheck';

const app = express();

app.use(logger('combined'));
app.use(json());
app.use(urlencoded({ extended: false }));
// app.use(cookieParser());

app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/healthcheck', hcRouter);

// catch 404 and forward to error handler
app.use((_req, _res, next) => {
  next(createError(404));
});

// error handler
app.use((err, req, res) => {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

/**
 * Get port from environment and store in Express.
 */
const port = process.env.PORT || '3000';
app.listen(port, () => {
  console.log('started');
});
